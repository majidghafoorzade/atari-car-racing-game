export default {
    methods: {
        CheckKey(e){
            e = e || window.event;
            if(e.keyCode == '37'){
                this.GoLeft();
            }else if(e.keyCode == '39'){
                this.GoRight();
            }
        }
    }
}